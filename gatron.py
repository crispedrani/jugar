import pyglet
from pyglet.window import key 
import math
from pyglet.gl import *

glEnable(GL_BLEND)
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

width = 960
height = 540
window = pyglet.window.Window(width, height)

raton = pyglet.resource.image("ratonpng.png")
gato = pyglet.resource.image("michiverde.png")
radio = 30

y = 0
x = 0
x_g = 900
y_g = 500

@window.event
def on_draw() :
    global x, y
    global x_g, y_g
    window.clear()
    raton.blit(x, y)
    gato.blit (x_g, y_g)

@window.event
def on_key_press(symbol, modifiers) :
    global x
    global y
    global x_g
    global y_g
    
    if symbol == key.LEFT:
        x_g = x_g - 10

    if symbol == key.DOWN :
        y_g = y_g - 10 

    if symbol == key.UP :
        y_g = y_g + 10

    if symbol == key.RIGHT : 
        x_g = x_g + 10

                   
 
    
    if symbol == key.D :
        x = x + 10
       
    if symbol == key.W :
        y = y + 10
    
    if symbol == key.S :
        y = y - 10
    
    if symbol == key.A :
        x = x - 10





@window.event
def update(dt) :
    global y_g, x_g, x, y 
    c1 = y_g - y
    c2 = x_g - x
    d = math.sqrt(math.pow(c1,2) + math.pow(c2, 2))
    if d < 2 * radio :
        panico(x, y)


#@window.event
def panico(x, y):
    for i in range(4):
        y = y + math.pow(1,i) * 10
        x = x + math.pow(1,i) * 10



pyglet.clock.schedule_interval(update, 1/60.0)
pyglet.app.run()

