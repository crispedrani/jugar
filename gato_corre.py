import pyglet
from pyglet.gl import *

glEnable(GL_BLEND)
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

ancho = 960
alto = 540

window = pyglet.window.Window(ancho, alto)

gatitos_q = [pyglet.resource.image('gatito.png'),
            pyglet.resource.image('gatito_1.png')]
        
fondo = pyglet.resource.image('fondo.jpg')
gatitos_img = [pyglet.resource.image('gatito_2.png'),
          pyglet.resource.image('gatito_3.png'),
          pyglet.resource.image('gatito_4.png')]
ani = pyglet.image.Animation.from_image_sequence(gatitos_q,duration=0.1, loop=False)
animacion = pyglet.image.Animation.from_image_sequence(gatitos_img, duration=0.5, loop=True)
spr = pyglet.sprite.Sprite(ani)
sprite = pyglet.sprite.Sprite(animacion)


x = ancho / 2
y = alto / 2

velocidad_x = 100
velocidad_y = 10

@window.event
def on_draw():
    window.clear()
    fondo.blit(0, 0)
    spr.draw()
    window.clear()
    fondo.blit(0, 0)
    sprite.draw()


@window.event
def update(dt):
    global velocidad_x, velocidad_y
    if sprite.x < ancho:
        sprite.x += velocidad_x * dt
    else:
        sprite.x = 0
    if sprite.y < alto:
        sprite.y += velocidad_y * dt
    else:
        sprite.y = 0

pyglet.clock.schedule_interval(update, 1/60.0)
pyglet.app.run()
