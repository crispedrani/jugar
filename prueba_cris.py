import pyglet

window = pyglet.window.Window()
image = pyglet.resource.image("auto2.png")
y = 0
@window.event
def on_draw() :
    window.clear()
    image.blit(10,y)
@window.event
def on_key_press(symbol, modifiers):
    global y
    y = y + 1

pyglet.app.run()
